require 'pgn'

p "Processing Seeds"

def process(game)
  if game.result == '1-0'
    evaluation = 1
  end
  if game.result == '1/2-1/2'
    evaluation = 0.5
  end
  if game.result == '0.-1'
    evaluation = 0
  end

  studies = game.fen_list.zip(game.moves.map(&:notation))
  studies.select.with_index { |_, i| i.even? }.each do |fen, move|
    fen_model = FEN.parse(fen)
    fen_model.move = move
    fen_model.study = true
    fen_model.evaluation = evaluation
    fen_model.save
  end
end

Dir["#{Rails.root}/seeds/*.pgn"].each do |file|
  File.open(file) do |f|
    games = PGN.parse(f.read)

    games.each do |game|
      process(game)
    end
  end
end

