Rails.application.routes.draw do
  root to: 'fens#index'
  get '/random', to: 'fens#random_puzzle'

  resources :users, only: [:create]
  post '/login', to: 'users#login'

  post '/studies/:id', to: 'fens#attempt'
end
