FROM ruby

RUN apt-get update -qq && apt-get install -y nodejs

RUN mkdir /zwischenzug_api
WORKDIR /zwischenzug_api

COPY Gemfile /zwischenzug_api/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
COPY . /zwischenzug_api

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]
