class StudyAttemptsRecord
  include Mongoid::Document

  embedded_in :user
  belongs_to :fen

  field :success, type: Boolean
end
