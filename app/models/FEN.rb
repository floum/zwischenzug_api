class FEN
  include Mongoid::Document

  store_in collection: 'fens'

  field :position, type: String
  field :turn, type: String
  field :castles, type: String
  field :en_passant, type: String
  field :study, type: Boolean
  field :evaluation, type: Float
  field :move, type: String

  def self.studies
    self.where(study: true)
  end

  def self.random_study
    self.studies.sample(1).first
  end

  def self.parse(fen)
    FEN.new(position: fen.split[0], turn: fen.split[1], castles: fen.split[2], en_passant: fen.split[3])
  end
end
