class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  
  MAX_POOL_SIZE = 30

  field :username, type: String
  field :password_digest, type: String
  field :mastered_studies_ids, type: Array, default: []

  has_secure_password

  embeds_many :study_attempts_records

  def current_studies
    study_attempts_records.map(&:fen).uniq
  end

  def random_pick_chance
    1 - current_studies.size / MAX_POOL_SIZE
  end

  def pick_study
    if Random.rand < random_pick_chance
      FEN.random_study
    else
      study_attempts_records.shuffle.first.fen
    end
  end

  def mastered?(study)
    mastered_studies_ids.include? study.id
  end
  
  def register_attempt(study, success)
    return if mastered?(study)

    study_attempts_records << StudyAttemptsRecord.new(fen: study, success: success)
    perform_mastery_check(study)
    save
  end

  def perform_mastery_check(study)
    attempts = study_attempts_records.select do |s|
      s.fen == study
    end

    if attempts.size > 5 && attempts.select { |a| a.success }.size.to_f / attempts.size > 0.8
      mastered_studies_ids << study.id
      attempts.each(&:delete)
    end
  end
end
