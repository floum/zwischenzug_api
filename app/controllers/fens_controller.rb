class FensController < ApplicationController
  before_action :authorized

  def index
    @fens = FEN.only_moves
    render json: @fens
  end

  def random_puzzle
    @fen = FEN.random_study
    @fen = @user.pick_study
    render json: @fen
  end

  def attempt
    @fen = FEN.find(params[:id])
    success = (@fen.move == params[:move])

    @user.register_attempt(@fen, success)
    render json: {success: success}
  end
end
